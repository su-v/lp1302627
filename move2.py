#!/usr/bin/env python
"""
move2.py - gtk2-based demo app to test window position after move()

Copyright 2016 su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import pygtk
pygtk.require('2.0')
import gtk

# globals
_VERBOSE = False


class MoveMe(object):

    def __init__(self):
        """Create main window for 'move' demo app."""
        # ----- display -----
        screen_width = gtk.gdk.screen_width()
        screen_height = gtk.gdk.screen_height()
        #
        windowing = gtk.gdk.WINDOWING
        # ----- toplevel window -----
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_gravity(gtk.gdk.GRAVITY_NORTH_WEST)
        self.window.set_decorated(True)
        self.window.set_title("Move")
        self.window.set_border_width(5)
        # ----- main box -----
        main_box = gtk.VBox(False, 0)
        # ----- screen info -----
        label_backend = gtk.Label()
        label_backend.set_label("GTK+ backend:")
        label_backend.set_alignment(0, 0.5)
        label_screen_w = gtk.Label()
        label_screen_w.set_label("Screen width:")
        label_screen_w.set_alignment(0, 0.5)
        label_screen_h = gtk.Label()
        label_screen_h.set_label("Screen height:")
        label_screen_h.set_alignment(0, 0.5)
        #
        backend = gtk.Label()
        backend.set_alignment(1, 0.5)
        screen_w = gtk.Label()
        screen_w.set_alignment(1, 0.5)
        screen_h = gtk.Label()
        screen_h.set_alignment(1, 0.5)
        #
        event_box_backend = gtk.EventBox()
        event_box_backend.add(backend)
        #
        box_backend = gtk.HBox()
        box_backend.add(label_backend)
        box_backend.pack_end(event_box_backend, False, False, 0)
        box_screen_w = gtk.HBox()
        box_screen_w.add(label_screen_w)
        box_screen_w.add(screen_w)
        box_screen_h = gtk.HBox()
        box_screen_h.add(label_screen_h)
        box_screen_h.add(screen_h)
        #
        box_info = gtk.VBox()
        box_info.add(box_backend)
        box_info.add(box_screen_w)
        box_info.add(box_screen_h)
        #
        align_info = gtk.Alignment(0, 0, 1, 0)
        align_info.add(box_info)
        # ----- position info -----
        # TODO: convert into table layout (as exercise)
        label_getpos_x = gtk.Label()
        label_getpos_x.set_label("get_position x:")
        label_getpos_x.set_alignment(0, 0.5)
        label_getpos_y = gtk.Label()
        label_getpos_y.set_label("get_position y:")
        label_getpos_y.set_alignment(0, 0.5)
        label_getsize_w = gtk.Label()
        label_getsize_w.set_label("Window width:")
        label_getsize_w.set_alignment(0, 0.5)
        label_getsize_h = gtk.Label()
        label_getsize_h.set_label("Window height:")
        label_getsize_h.set_alignment(0, 0.5)
        #
        getpos_x = gtk.Label()
        getpos_x.set_alignment(1, 0.5)
        getpos_y = gtk.Label()
        getpos_y.set_alignment(1, 0.5)
        getsize_w = gtk.Label()
        getsize_w.set_alignment(1, 0.5)
        getsize_h = gtk.Label()
        getsize_h.set_alignment(1, 0.5)
        #
        box_getpos_x = gtk.HBox()
        box_getpos_x.add(label_getpos_x)
        box_getpos_x.add(getpos_x)
        box_getpos_y = gtk.HBox()
        box_getpos_y.add(label_getpos_y)
        box_getpos_y.add(getpos_y)
        box_getsize_w = gtk.HBox()
        box_getsize_w.add(label_getsize_w)
        box_getsize_w.add(getsize_w)
        box_getsize_h = gtk.HBox()
        box_getsize_h.add(label_getsize_h)
        box_getsize_h.add(getsize_h)
        #
        box_oldpos = gtk.VBox()
        box_oldpos.add(box_getsize_w)
        box_oldpos.add(box_getsize_h)
        box_oldpos.add(box_getpos_x)
        box_oldpos.add(box_getpos_y)
        #
        align_oldpos = gtk.Alignment(0, 0, 1, 0)
        align_oldpos.add(box_oldpos)
        # ----- new position (spinbuttons) -----
        label_setpos_x = gtk.Label()
        label_setpos_x.set_label("move to x:")
        label_setpos_x.set_alignment(0, 0.5)
        label_setpos_y = gtk.Label()
        label_setpos_y.set_label("move to y:")
        label_setpos_y.set_alignment(0, 0.5)
        #
        adj_setpos_x = gtk.Adjustment(100, -100, screen_width, 1, 1)
        setpos_x = gtk.SpinButton(adj_setpos_x, 0, 0)
        adj_setpos_y = gtk.Adjustment(100, -100, screen_height, 1, 1)
        setpos_y = gtk.SpinButton(adj_setpos_y, 0, 0)
        #
        align_setpos_x = gtk.Alignment(1, 0, 0, 0)
        align_setpos_x.add(setpos_x)
        align_setpos_y = gtk.Alignment(1, 0, 0, 0)
        align_setpos_y.add(setpos_y)
        #
        box_setpos_x = gtk.HBox()
        box_setpos_x.add(label_setpos_x)
        box_setpos_x.add(align_setpos_x)
        box_setpos_y = gtk.HBox()
        box_setpos_y.add(label_setpos_y)
        box_setpos_y.add(align_setpos_y)
        #
        box_newpos = gtk.VBox()
        box_newpos.add(box_setpos_x)
        box_newpos.add(box_setpos_y)
        #
        align_newpos = gtk.Alignment(0, 0, 1, 0)
        align_newpos.add(box_newpos)
        # ----- options (boolean) -----
        check_gravity = gtk.CheckButton(use_underline=False)
        check_gravity.set_label("use GRAVITY_STATIC")
        check_decoration = gtk.CheckButton(use_underline=False)
        check_decoration.set_label("hide window decoration")
        check_verbose = gtk.CheckButton(use_underline=False)
        check_verbose.set_label("verbose")
        #
        box_checks = gtk.VBox()
        box_checks.add(check_gravity)
        box_checks.add(check_decoration)
        box_checks.add(check_verbose)
        #
        align_checks = gtk.Alignment(0, 0, 1, 0)
        align_checks.add(box_checks)
        # ----- commands (buttons) -----
        move = gtk.Button("Move")
        quit = gtk.Button(stock=gtk.STOCK_QUIT)
        #
        box_buttons = gtk.HBox(True, 0)
        box_buttons.add(move)
        box_buttons.add(quit)
        #
        align_buttons = gtk.Alignment(1, 0, 0, 0)
        align_buttons.add(box_buttons)
        # ----- vertical spacer (?) -----
        align_spacer = gtk.Alignment(1, 1, 0, 0)
        # ----- main_box -----
        main_box.pack_start(align_info, False, False, 5)
        main_box.pack_start(align_oldpos, False, False, 5)
        main_box.pack_start(align_newpos, False, False, 5)
        main_box.pack_start(align_checks, False, False, 5)
        main_box.pack_start(align_spacer, True, True, 0)
        main_box.pack_start(align_buttons, False, False, 0)
        #
        self.window.add(main_box)
        # ----- Signals -----
        self.window.connect("destroy", gtk.main_quit)
        self.window.connect("configure_event", self.on_configure,
                            screen_w, screen_h,
                            getpos_x, getpos_y,
                            getsize_w, getsize_h)
        #
        adj_setpos_x.connect("value_changed", self.on_move, setpos_x, setpos_y)
        adj_setpos_y.connect("value_changed", self.on_move, setpos_x, setpos_y)
        setpos_y.connect("activate", self.on_move, setpos_x, setpos_y)
        setpos_x.connect("activate", self.on_move, setpos_x, setpos_y)
        #
        check_gravity.connect("clicked", self.on_gravity_check)
        check_decoration.connect("clicked", self.on_decoration)
        check_verbose.connect("clicked", self.on_verbose)
        #
        move.connect("clicked", self.on_move, setpos_x, setpos_y)
        quit.connect("clicked", gtk.main_quit)
        # fill in backend info
        backend.set_text(windowing)
        if windowing == "quartz":
            backend_bg = gtk.gdk.Color(1.0, 0.7, 0.7)
        elif windowing == "x11":
            backend_bg = gtk.gdk.Color(0.7, 0.7, 1.0)
        else:
            backend_bg = None
        event_box_backend.modify_bg(gtk.STATE_NORMAL, backend_bg)
        # all set up, now show it!
        self.window.show_all()

    def on_configure(self, widget, event, sw, sh, wx, wy, ww, wh):
        """Update screen size and window position on configure event."""
        sw.set_text(str(gtk.gdk.screen_width()))
        sh.set_text(str(gtk.gdk.screen_height()))
        x, y = self.window.get_position()
        w, h = self.window.get_size()
        wx.set_text(str(x))
        wy.set_text(str(y))
        ww.set_text(str(w))
        wh.set_text(str(h))
        if _VERBOSE:
            print("----- on_configure_event")
            print("get_position()      ", (x, y))
            print("get_root_origin()   ",
                  gtk.gdk.Window.get_root_origin(self.window.get_window()))
            frame = gtk.gdk.Window.get_frame_extents(self.window.get_window())
            print("get_frame_extents() ",
                  (frame.x, frame.y, frame.width, frame.height))
            print("get_origin()        ",
                  gtk.gdk.Window.get_origin(self.window.get_window()))
            print("get_geometry()      ",
                  gtk.gdk.Window.get_geometry(self.window.get_window())[:4])

    def on_move(self, widget, spin_x, spin_y):
        """Move window to new position taken from user input."""
        x = spin_x.get_value_as_int()
        y = spin_y.get_value_as_int()
        self.window.move(x, y)
        if _VERBOSE:
            print("----- on_move")
            print("move to", (x, y))

    def on_decoration(self, widget):
        """Check and set window decoration."""
        x, y = self.window.get_position()
        self.window.hide()
        if widget.get_active():
            self.window.set_decorated(False)
        else:
            self.window.set_decorated(True)
        self.window.move(x, y)
        self.window.present()

    def on_gravity_check(self, widget):
        """Check and set window gravity."""
        w, h = self.window.get_size()
        if widget.get_active():
            # trigger configure event
            self.window.set_gravity(gtk.gdk.GRAVITY_STATIC)
            self.window.resize(w, h+1)
        else:
            # trigger configure event
            self.window.set_gravity(gtk.gdk.GRAVITY_NORTH_WEST)
            self.window.resize(w, h-1)

    def on_verbose(self, widget):
        """Check and set verbose mode (console output)."""
        global _VERBOSE
        if widget.get_active():
            _VERBOSE = True
        else:
            _VERBOSE = False

    def main(self):
        """Start Gtk main loop."""
        gtk.main()


if __name__ == "__main__":
    ME = MoveMe()
    ME.main()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79

#!/usr/bin/env python3
"""
maximize3.py - gtk3-based demo app to test maximized window state event

Copyright 2016 su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk


class Maximize(object):

    def on_delete_event(self, widget, event, data=None):
        return False

    def on_destroy(self, widget, data=None):
        Gtk.main_quit()

    def toggle_maximize(self, widget):
        if widget.get_active():
            self.window.maximize()
        else:
            self.window.unmaximize()

    def on_window_state_event(self, widget, event):
        if event.changed_mask & Gdk.WindowState.MAXIMIZED:
            if event.new_window_state & Gdk.WindowState.MAXIMIZED:
                widget.set_text("Maximized")
            else:
                widget.set_text("Unmaximized")
        else:
            widget.set_text("Unchanged")

    def __init__(self):
        """Create main window for 'maximize' demo app."""
        self.window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        self.window.set_title("Test Event")
        self.window.set_border_width(10)

        vbox = Gtk.VBox(False, 0)
        self.window.add(vbox)

        hbox = Gtk.HBox(False, 5)
        vbox.pack_start(hbox, False, False, 0)

        label = Gtk.Label()
        label.set_text("Window state:")
        hbox.pack_start(label, False, False, 0)

        state = Gtk.Label()
        state.set_text("NA")
        state.set_alignment(0, 0)
        hbox.pack_start(state, False, False, 0)

        hbox = Gtk.HBox(False, 0)
        vbox.pack_start(hbox, False, False, 0)

        button = Gtk.CheckButton()
        button.set_label("Maximize Window")
        button.connect("toggled", self.toggle_maximize)
        hbox.pack_start(button, False, False, 0)

        self.window.show_all()

        self.window.connect("delete_event", self.on_delete_event)
        self.window.connect("destroy", self.on_destroy)

        # Signal handler in Inkscape (src/interface.cpp):
        # win->signal_window_state_event().connect(sigc::mem_fun(*desktop, &SPDesktop::onWindowStateEvent));
        # Equivalent handler with python bindings based on:
        # * 10.22. How can I find out when my GtkWindow is minimized?
        #   http://faq.pygtk.org/index.py?req=show&file=faq10.022.htp
        self.window.connect_object("window-state-event",
                                   self.on_window_state_event,
                                   state)

    def main(self):
        """Start Gtk main loop."""
        Gtk.main()


if __name__ == "__main__":
    APP = Maximize()
    APP.main()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79

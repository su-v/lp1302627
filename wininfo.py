#!/usr/bin/env python
"""
wininfo.py - gtk2-based demo app to test window position after move()

Copyright 2016 su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=invalid-name
# pylint: disable=bad-whitespace
# pylint: disable=missing-docstring
# pylint: disable=no-member

# standard library
import sys
if sys.version_info < (3,):
    import pygtk
    pygtk.require('2.0')
    import gtk
else:
    sys.stderr.write("Python 3 with PyGObject not (yet) supported.\n")
    sys.exit(1)


# globals
_VERBOSE = False


def showme(foo):
    if _VERBOSE:
        # sys.stderr.write(str(type(foo)))
        sys.stderr.write(str(foo) + "\n")


def currentFuncName(n=0):
    return sys._getframe(n + 1).f_code.co_name


def get_gtk_backend():
    windowing = gtk.gdk.WINDOWING
    return windowing


def get_screen_size():
    width = gtk.gdk.screen_width()
    height = gtk.gdk.screen_height()
    return (width, height)


def get_work_area():
    pass  # requires gtk3


def align_r(widget):
    widget.set_alignment(1, 0.5)
    return widget


def align_c(widget):
    widget.set_alignment(0.5, 0.5)
    return widget


def align_l(widget):
    widget.set_alignment(0, 0.5)
    return widget


def align_h(widget):
    widget.set_alignment(0.5, 0.5)
    return widget


def add_lm(table, col, row, string="NA", store=None):
    """Add label to table cell, left-aligned with markup."""
    label = gtk.Label()
    label.set_markup(string)
    table.attach(align_l(label), col-1, col+1, row-1, row+1)
    if store is not None:
        d, name = store
        d[name] = label


def add_lt(table, col, row, string="NA", store=None):
    """Add label to table cell, left-aligned with plain text."""
    label = gtk.Label()
    label.set_text(string)
    table.attach(align_l(label), col-1, col+1, row-1, row+1)
    if store is not None:
        d, name = store
        d[name] = label


def add_rm(table, col, row, string="NA", store=None):
    """Add label to table cell, right-aligned with markup."""
    label = gtk.Label()
    label.set_markup(string)
    table.attach(align_r(label), col-1, col+1, row-1, row+1)
    if store is not None:
        d, name = store
        d[name] = label


def add_rt(table, col, row, string="NA", store=None):
    """Add label to table cell, right-aligned with plain text."""
    label = gtk.Label()
    label.set_text(string)
    table.attach(align_r(label), col-1, col+1, row-1, row+1)
    if store is not None:
        d, name = store
        d[name] = label


def create_info_table(data):
    """Create table to display info about window position and size."""
    # pylint: disable=too-many-statements
    # ----- init table widget -----
    it = gtk.Table()
    it.set_homogeneous(False)
    it.resize(13, 6)
    it.set_row_spacings(15)
    it.set_col_spacings(40)
    it.set_col_spacing(1, 0)
    it.set_name("infotable")
    it.set_border_width(5)
    # ----- known columns -----
    cc = 1  # context
    cx = 3  # x
    cy = 4  # y
    cw = 5  # width
    ch = 6  # height
    # ----- known rows -----
    r = 1   # first
    # ----- content -----
    data['backend'] = gtk.Label()
    data['backend'].set_label("TODO")
    # EventBox for backend to allow coloring bg depending on value
    data['backend_bg'] = gtk.EventBox()
    data['backend_bg'].add(align_c(data['backend']))
    it.attach(data['backend_bg'], cc-1, cc+1, r-1, r+1)
    #
    add_rt(it, cx, r, "X")
    add_rt(it, cy, r, "Y")
    add_rt(it, cw, r, "Width")
    add_rt(it, ch, r, "Height")
    r += 1  # ----- row -----
    r += 1  # ----- row -----
    add_lm(it, cc, r, "<b>Root</b>")
    r += 1  # ----- row -----
    add_lt(it, cc, r, "screen")
    add_rt(it, cw, r, store=(data, 'screen_w'))
    add_rt(it, ch, r, store=(data, 'screen_h'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "workarea")
    add_rt(it, cw, r, store=(data, 'workarea_w'))
    add_rt(it, ch, r, store=(data, 'workarea_h'))
    r += 1  # ----- row -----
    r += 1  # ----- row -----
    add_lm(it, cc, r, "<b>Window</b>")
    r += 1  # ----- row -----
    add_lt(it, cc, r, "maximized")
    add_rt(it, cx, r, store=(data, 'maximized'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "get_pos")
    add_rt(it, cx, r, store=(data, 'window_x'))
    add_rt(it, cy, r, store=(data, 'window_y'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "get_size")
    add_rt(it, cw, r, store=(data, 'window_w'))
    add_rt(it, ch, r, store=(data, 'window_h'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "get_geometry")
    add_rt(it, cx, r, store=(data, 'geom_x'))
    add_rt(it, cy, r, store=(data, 'geom_y'))
    add_rt(it, cw, r, store=(data, 'geom_w'))
    add_rt(it, ch, r, store=(data, 'geom_h'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "get_origin")
    add_rt(it, cx, r, store=(data, 'origin_x'))
    add_rt(it, cy, r, store=(data, 'origin_y'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "get_root_origin")
    add_rt(it, cx, r, store=(data, 'root_origin_x'))
    add_rt(it, cy, r, store=(data, 'root_origin_y'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "get_frame_extents")
    add_rt(it, cx, r, store=(data, 'frame_extents_x'))
    add_rt(it, cy, r, store=(data, 'frame_extents_y'))
    add_rt(it, cw, r, store=(data, 'frame_extents_w'))
    add_rt(it, ch, r, store=(data, 'frame_extents_h'))
    r += 1  # ----- row -----
    r += 1  # ----- row -----
    add_lm(it, cc, r, "<b>Pointer</b>")
    r += 1  # ----- row -----
    add_lt(it, cc, r, "window")
    add_rt(it, cx, r, store=(data, 'pointer_x'))
    add_rt(it, cy, r, store=(data, 'pointer_y'))
    r += 1  # ----- row -----
    add_lt(it, cc, r, "root")
    add_rt(it, cx, r, store=(data, 'pointer_root_x'))
    add_rt(it, cy, r, store=(data, 'pointer_root_y'))
    #
    box = gtk.HBox(False, 0)
    table_bg = gtk.EventBox()
    table_bg.add(it)
    table_bg.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(0.95, 0.95, 0.95))
    box.pack_start(table_bg, False, False, 0)
    box.show_all()
    return box


def create_position_input(data):
    """Create box with spinbuttons for new position (x, y)."""
    label_x = gtk.Label()
    label_x.set_label("Move window to x:")
    label_x.set_alignment(0, 0.5)
    label_y = gtk.Label()
    label_y.set_label("Move window to y:")
    label_y.set_alignment(0, 0.5)
    #
    data['adj_x'] = gtk.Adjustment(100, -100, get_screen_size()[0], 1, 1)
    data['set_x'] = gtk.SpinButton(data['adj_x'], 0, 0)
    data['adj_y'] = gtk.Adjustment(100, -100, get_screen_size()[1], 1, 1)
    data['set_y'] = gtk.SpinButton(data['adj_y'], 0, 0)
    #
    vbox = gtk.VBox()
    #
    hbox = gtk.HBox(False, 20)
    hbox.pack_start(label_x, False, False, 0)
    hbox.pack_end(data['set_x'], False, False, 0)
    vbox.add(hbox)
    hbox = gtk.HBox(False, 20)
    hbox.pack_start(label_y, False, False, 0)
    hbox.pack_end(data['set_y'], False, False, 0)
    vbox.add(hbox)
    #
    box = gtk.HBox(False, 0)
    box.pack_start(vbox, False, False, 0)
    box.show_all()
    return box


def create_size_input(data):
    """Create box with spinbuttons for new size (w, h)."""
    label_w = gtk.Label()
    label_w.set_label("New window width:")
    label_w.set_alignment(0, 0.5)
    label_h = gtk.Label()
    label_h.set_label("New window height:")
    label_h.set_alignment(0, 0.5)
    #
    data['adj_w'] = gtk.Adjustment(100, 1, get_screen_size()[0], 1, 1)
    data['set_w'] = gtk.SpinButton(data['adj_w'], 0, 0)
    data['adj_h'] = gtk.Adjustment(100, 1, get_screen_size()[1], 1, 1)
    data['set_h'] = gtk.SpinButton(data['adj_h'], 0, 0)
    #
    vbox = gtk.VBox()
    #
    hbox = gtk.HBox(False, 20)
    hbox.pack_start(label_w, False, False, 0)
    hbox.pack_end(data['set_w'], False, False, 0)
    vbox.add(hbox)
    hbox = gtk.HBox(False, 20)
    hbox.pack_start(label_h, False, False, 0)
    hbox.pack_end(data['set_h'], False, False, 0)
    vbox.add(hbox)
    #
    box = gtk.HBox(False, 0)
    box.pack_start(vbox, False, False, 0)
    box.show_all()
    return box


def create_options(data):
    """Create box with checkbuttons for boolean options."""
    data['gravity'] = gtk.CheckButton(use_underline=False)
    data['gravity'].set_label("use GRAVITY_STATIC")
    data['decoration'] = gtk.CheckButton(use_underline=False)
    data['decoration'].set_label("hide window decoration")
    data['maximize'] = gtk.CheckButton(use_underline=False)
    data['maximize'].set_label("maximize window")
    data['quartz_hack'] = gtk.CheckButton(use_underline=False)
    data['quartz_hack'].set_label("fix gtk.Window.move() with Quartz")
    data['verbose'] = gtk.CheckButton(use_underline=False)
    data['verbose'].set_label("verbose")
    #
    vbox = gtk.VBox()
    vbox.pack_start(data['gravity'], False, False, 0)
    vbox.pack_start(data['decoration'], False, False, 0)
    vbox.pack_start(data['maximize'], False, False, 0)
    vbox.pack_start(data['quartz_hack'], False, False, 0)
    vbox.pack_start(data['verbose'], False, False, 0)
    #
    box = gtk.HBox(False, 0)
    box.pack_start(vbox, False, False, 0)
    box.show_all()
    return box


def create_commands(data):
    """Create box with buttons for commands."""
    data['roundtrip'] = gtk.Button(use_underline=False)
    data['roundtrip'].set_label("Move to get_pos")
    data['move'] = gtk.Button(use_underline=False)
    data['move'].set_label("Move to x, y")
    data['quit_'] = gtk.Button(stock=gtk.STOCK_QUIT)
    #
    box = gtk.HBox(False, 0)
    box.pack_end(data['quit_'], False, False, 0)
    box.pack_end(data['move'], False, False, 0)
    box.pack_end(data['roundtrip'], False, False, 0)
    #
    box.show_all()
    return box


def create_spacer(halign, valign):
    """Create empty alignment as spacer element."""
    p1 = bool(halign == 'right')
    p2 = bool(valign == 'bottom')
    p3 = p4 = 0
    align = gtk.Alignment(p1, p2, p3, p4)
    #
    align.show()
    return align


def check_backend(data):
    data['quartz_hack'].set_sensitive(False)
    windowing = get_gtk_backend()
    if windowing == "quartz":
        color = gtk.gdk.Color(1.0, 0.7, 0.7)
        data['quartz_hack'].set_sensitive(True)
    elif windowing == "x11":
        color = gtk.gdk.Color(0.7, 0.7, 1.0)
    else:
        color = None
    data['backend'].set_text(windowing)
    data['backend_bg'].modify_bg(gtk.STATE_NORMAL, color)


def check_window_size(window, data):
    w = int(data['frame_extents_w'].get_text())
    h = int(data['frame_extents_h'].get_text())
    if _VERBOSE:
        print('width: {}'.format(w))
        print('height: {}'.format(h))
    data['set_w'].set_value(w)
    data['set_h'].set_value(h)


class WinInfo(object):

    def __init__(self):
        """Create main window for 'wininfo' demo app."""
        #
        # ----- common -----
        #
        data = {}
        self.use_quartz_hack = False
        #
        # ----- main window -----
        #
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.add_events(gtk.gdk.POINTER_MOTION_MASK |
                               gtk.gdk.POINTER_MOTION_HINT_MASK)
        #
        # ----- main window props -----
        #
        self.window.set_title("Window Info")
        self.window.set_gravity(gtk.gdk.GRAVITY_NORTH_WEST)
        self.window.set_decorated(True)
        self.window.set_border_width(10)
        self.window.set_name("wininfo")
        #
        # ----- create widgets -----
        #
        main_box = gtk.VBox(False, 0)
        #
        box_info = create_info_table(data)
        box_newpos = create_position_input(data)
        box_newsize = create_size_input(data)
        box_options = create_options(data)
        box_buttons = create_commands(data)
        #
        # ----- add/pack widgets -----
        #
        main_box.pack_start(box_info, False, False, 5)
        main_box.pack_start(box_newpos, False, False, 5)
        main_box.pack_start(box_newsize, False, False, 5)
        main_box.pack_start(box_options, False, False, 5)
        #
        main_box.pack_end(box_buttons, False, False, 0)
        #
        main_box.show()
        #
        # ----- signals/events -----
        #
        self.window.connect("delete_event", self.on_delete_event)
        self.window.connect("destroy", gtk.main_quit)
        self.window.connect("configure_event", self.on_configure_event, data)
        if 1:
            self.window.connect_object("motion-notify-event",
                                       self.on_motion_event_top,
                                       self.window,
                                       data)
        else:
            self.window.connect("motion-notify-event",
                                self.on_motion_event,
                                data)
        self.window.connect("window-state-event",
                            self.on_window_state_event,
                            data)
        #
        data['adj_x'].connect("value_changed", self.on_pos_changed, data)
        data['adj_y'].connect("value_changed", self.on_pos_changed, data)
        data['set_x'].connect("activate", self.on_move, data)
        data['set_y'].connect("activate", self.on_move, data)
        #
        data['adj_w'].connect("value_changed", self.on_size_changed, data)
        data['adj_h'].connect("value_changed", self.on_size_changed, data)
        data['set_w'].connect("activate", self.on_size_changed, data)
        data['set_h'].connect("activate", self.on_size_changed, data)
        #
        data['gravity'].connect("toggled", self.toggle_gravity)
        data['decoration'].connect("toggled", self.toggle_decoration)
        data['maximize'].connect("toggled", self.toggle_maximize)
        data['quartz_hack'].connect("toggled", self.toggle_quartz_hack)
        data['verbose'].connect("toggled", self.toggle_verbose)
        #
        data['roundtrip'].connect("clicked", self.on_roundtrip, data)
        data['move'].connect("clicked", self.on_move, data)
        data['quit_'].connect("clicked", gtk.main_quit)
        #
        # ----- backend info -----
        #
        check_backend(data)
        #
        # ----- show -----
        #
        self.window.add(main_box)
        # self.window.maximize()
        # data['maximize'].set_active(True)
        # data['maximize'].toggled()
        # data['maximized'].set_text("Yes")
        data['maximized'].set_text("No")
        self.window.show()

    def on_delete_event(self, widget, event, data=None):
        """Event handler for WM closing the window."""
        gtk.main_quit()
        return True

    def on_configure_event(self, _, event, data):
        """Update info table on configure event."""
        if _VERBOSE:
            print('--- {}'.format(currentFuncName()))
            # print(dir(event))
            print('coords: {}'.format(event.get_coords()))
            print('size: {}x{}'.format(event.width, event.height))
        self.update_info(data)

    def on_motion_event(self, widget, event, data):
        """Update pointer position in window and root coordinates."""
        showme('--- {}'.format(currentFuncName()))
        showme(widget.name)
        # showme(dir(event))
        if event.window is not self.window.get_window():
            # spinbuttons have own window with this event added
            # use coords from top-level window instead
            x, y = self.window.get_pointer()
        else:
            # use coords as returned by event for widget
            x, y = event.get_coords()
        showme('coords:     {: 8.1f},{: 8.1f}'.format(x, y))
        data['pointer_x'].set_text(str(int(x)))
        data['pointer_y'].set_text(str(int(y)))
        x, y = event.get_root_coords()
        showme('root coords:{: 8.1f},{: 8.1f}'.format(x, y))
        data['pointer_root_x'].set_text(str(int(x)))
        data['pointer_root_y'].set_text(str(int(y)))

    def on_motion_event_top(self, widget, event, data):
        """Update pointer position in window and root coordinates."""
        showme('--- {}'.format(currentFuncName()))
        showme(widget.name)
        # showme(dir(event))
        x, y = widget.get_pointer()
        showme('coords:     {: 8.1f},{: 8.1f}'.format(x, y))
        data['pointer_x'].set_text(str(int(x)))
        data['pointer_y'].set_text(str(int(y)))
        x, y = event.get_root_coords()
        showme('root coords:{: 8.1f},{: 8.1f}'.format(x, y))
        data['pointer_root_x'].set_text(str(int(x)))
        data['pointer_root_y'].set_text(str(int(y)))

    def on_window_state_event(self, widget, event, data):
        """Update window state info."""
        self.update_info(data)
        if _VERBOSE:
            print('--- {}'.format(currentFuncName()))
            # print(dir(event))
            print('event type: {}'.format(event.type))
            print('changed_mask: {}'.format(event.changed_mask))
            print('new_window_state: {}'.format(event.new_window_state))
        if event.changed_mask & gtk.gdk.WINDOW_STATE_MAXIMIZED:
            if event.new_window_state & gtk.gdk.WINDOW_STATE_MAXIMIZED:
                if _VERBOSE:
                    print 'Window maximized!'
                data['maximized'].set_text("Yes")
            else:
                if _VERBOSE:
                    print 'Window unmaximized!'
                data['maximized'].set_text("No")
        else:
            if _VERBOSE:
                print 'Window unchanged!'
            data['maximized'].set_text("Unchanged")

    def on_roundtrip(self, widget, data):
        """Move window to position returned by get_position()."""
        x = int(data['window_x'].get_text())
        y = int(data['window_y'].get_text())
        data['set_x'].set_value(x)
        data['set_y'].set_value(y)
        if self.use_quartz_hack:
            y += 22
        if _VERBOSE:
            print('--- {}'.format(currentFuncName()))
            print('widget name: {}'.format(widget.name))
            print('move to: ({}, {})'.format(x, y))
        self.window.move(x, y)

    def on_pos_changed(self, widget, data):
        """Move window to new position taken from user input."""
        if _VERBOSE:
            print('--- {}'.format(currentFuncName()))
            print(widget.value)
        self.on_move(widget, data)

    def on_move(self, widget, data):
        """Move window to new position taken from user input."""
        x = data['set_x'].get_value_as_int()
        y = data['set_y'].get_value_as_int()
        if self.use_quartz_hack:
            y += 22
        if _VERBOSE:
            print('--- {}'.format(currentFuncName()))
            print('--- called by: {}'.format(currentFuncName(1)))
            print(type(widget))
            print('move to: ({}, {})'.format(x, y))
        self.window.move(x, y)

    def on_size_changed(self, widget, data):
        """Chnage window to new size taken from user input."""
        w = data['set_w'].get_value_as_int()
        h = data['set_h'].get_value_as_int()
        if _VERBOSE:
            print('--- {}'.format(currentFuncName()))
            print(widget.value)
            print('new size: ({}, {})'.format(w, h))
        self.window.resize(w, h)

    def toggle_gravity(self, widget):
        """Check and set window gravity."""
        # FIXME: how to trigger configure-event directly?
        w, h = self.window.get_size()
        if widget.get_active():
            self.window.set_gravity(gtk.gdk.GRAVITY_STATIC)
            # trigger configure event
            self.window.resize(w+1, h)
        else:
            self.window.set_gravity(gtk.gdk.GRAVITY_NORTH_WEST)
            # trigger configure event
            self.window.resize(w-1, h)

    def toggle_decoration(self, widget):
        """Check and set window decoration."""
        x, y = self.window.get_position()
        self.window.hide()
        self.window.set_decorated(not widget.get_active())
        self.window.move(x, y)
        self.window.present()

    def toggle_maximize(self, widget):
        """Check and set window decoration."""
        if widget.get_active():
            self.window.maximize()
        else:
            self.window.unmaximize()

    def toggle_quartz_hack(self, widget):
        """Check and set instance variable for quartz hack."""
        self.use_quartz_hack = bool(widget.get_active() and
                                    get_gtk_backend() == 'quartz')

    def toggle_verbose(self, widget):
        """Check and set verbose mode."""
        global _VERBOSE
        _VERBOSE = widget.get_active()

    def update_info(self, data):
        """Update various position and size values in info table."""
        if _VERBOSE:
            print('--- {}'.format(currentFuncName()))
        # screen
        w, h = get_screen_size()
        data['screen_w'].set_text(str(w))
        data['screen_h'].set_text(str(h))
        # workarea
        # TODO: find solution for gtk2
        # get_pos
        x, y = self.window.get_position()
        data['window_x'].set_text(str(x))
        data['window_y'].set_text(str(y))
        # get_size
        w, h = self.window.get_size()
        data['window_w'].set_text(str(w))
        data['window_h'].set_text(str(h))
        # get_geometry
        x, y, w, h = gtk.gdk.Window.get_geometry(self.window.get_window())[:4]
        data['geom_x'].set_text(str(x))
        data['geom_y'].set_text(str(y))
        data['geom_w'].set_text(str(w))
        data['geom_h'].set_text(str(h))
        # get_origin
        x, y = gtk.gdk.Window.get_origin(self.window.get_window())
        data['origin_x'].set_text(str(x))
        data['origin_y'].set_text(str(y))
        # get_root_origin
        x, y = gtk.gdk.Window.get_root_origin(self.window.get_window())
        data['root_origin_x'].set_text(str(x))
        data['root_origin_y'].set_text(str(y))
        # get_frame_extents
        frame = gtk.gdk.Window.get_frame_extents(self.window.get_window())
        data['frame_extents_x'].set_text(str(frame[0]))
        data['frame_extents_y'].set_text(str(frame[1]))
        data['frame_extents_w'].set_text(str(frame[2]))
        data['frame_extents_h'].set_text(str(frame[3]))
        # pointer window
        x, y = self.window.get_pointer()
        data['pointer_x'].set_text(str(int(x)))
        data['pointer_y'].set_text(str(int(y)))
        # pointer root
        x = y = 'NA'
        data['pointer_root_x'].set_text(x)
        data['pointer_root_y'].set_text(y)

    def main(self):
        """Start Gtk main loop."""
        # pylint: disable=no-self-use
        gtk.main()


if __name__ == "__main__":
    APP = WinInfo()
    APP.main()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
